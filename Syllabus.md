# CS-448 Software Development Capstone

## *CS-448 01, O2 &mdash; Spring 2025*

### Version 2025-Spring-1.0, Revised 20 January 2025

## Credit and Contact Hours

3 credits

Lecture: 3 hours/week

## Catalog Course Description

> *Development of a significant software system, following appropriate
> project and team management techniques. Requirements, design,
> implementation, quality assurance, professional, social and ethical
> issues.*

## Instructor

Dr. Karl R. Wurst

I prefer to be addressed, or referred to, as "Professor Wurst" or "Doctor
Wurst". My preferred pronouns are "he/him/his", although I am also fine
with "they/them/their".

See [https://www.karl.w-sts.com](https://www.karl.w-sts.com) for
contact information and schedule.

## Meeting Time and Location

- **Section 01:** Mondays and Wednesdays 12:30-13:45, Sullivan Room 105
- **Section O2:** Tuesdays and Thursdays 10:00-11:15, Sullivan Room 105
  
## It's in the Syllabus

**If you don't find the answer to your question in the syllabus, then
please ask me.**

![It's in the syllabus comic](http://www.phdcomics.com/comics/archive/phd051013s.gif)
[http://www.phdcomics.com/comics.php?f=1583](http://www.phdcomics.com/comics.php?f=1583)

## Textbooks

<!-- markdownlint-disable MD033 -->
<table cellpadding="1" border="0">
<tbody>
<tr>
<td align="center">
<img src="https://www.karl.w-sts.com/index_files/9780596518387.gif" width="110" />
</td>
<td><em>Apprenticeship Patterns: Guidance for the Aspiring Software
Craftsman</em>
<br> Dave Hoover and Adewale Oshineye
<br />O'Reilly Media, 2009
<br>ISBN-13: 9780596518387<br>
<a target="_blank" href="http://shop.oreilly.com/product/9780596518387.do">
At this time, the full text is available through this link.</a> (Scroll
down to the Table of Contents.)
</tr>
<tr>
<td align="center"><img src="https://www.karl.w-sts.com/index_files/scrum-intro-cover-front-outlined-400-207x300.jpg" width="110" /></td>
<td><em>Scrum: A Breathtakingly Brief and Agile Introduction</em>
<br /> Chris Sims and Hillary Louise Johnson
<br /> Dymaxicon, 2012
<br /> ISBN-13: 9781937965044
<br /> <a href="http://www.amazon.com/Scrum-Breathtakingly-Brief-Agile-Introduction-ebook/dp/B007P5N8D4/ref=tmm_kin_title_0?_encoding=UTF8&amp;sr=8-1&amp;qid=1333650002" target="_blank">Available as a $0.99 Kindle book</a><br /> <a href="http://www.agilelearninglabs.com/resources/scrum-introduction/" target="_blank">
Read the whole text online here</a></td>
</tr>
<tr>
<td align="center"><a title="By Michael Reschke (self-made, following OERCommons.org&#039;s example) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AOERlogo.svg"><img width="100" alt="OERlogo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/OERlogo.svg/256px-OERlogo.svg.png"/></a>
</td>
<td>We will also be using freely available learning resources for topics
in this course.
</td>
</tr>
</tbody>
</table>
<!-- markdownlint-enable MD033 -->

## Required Materials

In addition to the textbooks, to successfully complete this course you will
need:

1. **Laptop Computer:** You will need a laptop computer that you can bring
    to class sessions and can use at home. **You must bring your laptop to
    every class session.** The brand, specs, and operating system (Windows,
    Mac OS X, Linux) is unimportant.
2. **Internet Access:** You will need Internet access for access to:
    1. **GitLab** &mdash; We will use GitLab to host our code.
    2. **Discord** &mdash; We will use Discord for team communication.
    3. **TEAMMATES** &mdash; We will use TEAMMATES to reflect on and improve
        team performance and effectiveness.
    4. **Blogging site** &mdash; You will be blogging about your sprints and
        about your readings in *Apprenticeship Patterns: Guidance for the
        Aspiring Software Craftsman*
    5. **Tutorials and articles** &mdash; I will suggest, and you will
        research on your own, tutorials and articles for you to learn new
        technologies and techniques we need.

## Where Does This Course Lead?

- Your professional career

## Course Workload Expectations

***This is a three-credit project-based course. You should expect to spend,
on average, 9 hours per week on this class.***

You will spend 9 hours per week participating in Scrum meetings, working
with your team, writing code and documentation, learning new tools and
techniques, and communicating with the team. (See *Definition of the Credit
Hour*)

***The bulk of your work in this course will take place outside of the
class time. It is not possible to contribute the amount of work your team
and the class need to be successful entirely within the 3 hours of class
time. If you attempt to do so, you will not only receive a poor grade in
the course, but will also be letting down your classmates, and giving a
poor impression of your reliability and quality of work to your classmates
and to members of the professional community.***

***This is a chance to build your professional reputation - make sure you
make a good impression.***

### Attendance

You are required to attend a certain number of class periods, when you will
be meeting in your teams with the instructor (who is acting as Product
Owner for your project). You will need to work with your teammates at other
times on planning, review, estimation, task creation and selection,
learning, and implementation. It is important for all members of the team
to participate to enable the team to make progress. The instructor will be
available for all class periods even if they are not required. See
important details under Attendance and Participation to see how you will be
graded.

## Definition of the Credit Hour

> The Commission defines a credit hour as an amount of work represented in
> intended learning outcomes and verified by evidence of student achievement
> that is an institutional established equivalence that reasonably
> approximates not less than –
(1) One hour of classroom or direct faculty instruction and a minimum of
two hours of out of class student work each week for approximately fifteen
weeks for one semester or trimester hour of credit, or ten to twelve weeks
for one quarter hour of credit, or the equivalent amount of work over a
different amount of time; or
(2) At least an equivalent amount of work as required in paragraph (1) of
this definition for other academic activities as established by the
institution including laboratory work, internships, practica, studio work,
and other academic work leading to the award of credit hours.
>&mdash; New England Commission of Higher Education,
[Policy on Credits and Degrees](https://www.neche.org/wp-content/uploads/2018/12/Pp111_Policy_On_Credits-And-Degrees.pdf)

## Prerequisites

- CS 343 Software Construction, Design and Architecture
- CS 348 Software Process Management
- CS 373 Operating Systems (corequisite)
- CS 443 Software Quality Assurance and Testing (corequisite)

This course has a prerequisite of CS 343 &mdash; Software Construction,
Design and Architecture. I expect that you understand good software design
and construction principles, can read code and comprehend the design of a
system, can design and build software as part of a larger system using good
design principles.

This course has a prerequisite of CS 348 &mdash; Software Process
Management. You will either have some familiarity with the software
development process, with some team processes, collaboration tools, and
will use them as your team works on its project during this semester.

This course has a prerequisite or corequisite of CS 373 &mdash; Operating
Systems. I expect you to understand (or be learning) some of the problems
of large, concurrent systems that manage system resources, as the system we
are developing will be using concurrency.

This course has a prerequisite or corequisite of CS 443 &mdash; Software
Quality Assurance and Testing. I expect you to be able to develop and
implement appropriate test cases, and be able to use test automation
systems.

In addition, I expect you to have taken the following (or their equivalents):

- CS 282 &mdash; UNIX System Programming. I expect you to have some
  familiarity with command-line interfaces and low-level systems coding.
- CS 242 &mdash; Data Structures. I expect that you have a solid background
  in fundamental data structures, such as lists, queues and trees, know how
  to use them and how to implement them, and when to select a data
  structure for a particular problem. I also expect you to be a proficient
  coder and debugger.
- CM 110 &mdash; Public Speaking. You should be able to structure and give
  a professional-quality presentation.
- PH 134 &mdash; Computing Ethics or UR 230 – Technology, Public Policy and
  Urban Society. I expect that you are able to evaluate the course project
  for ethical and professional considerations, and write a well-constructed, well-reasoned, and well-supported analysis.
- EN 252 &mdash; Technical Writing. You should be able to write
  professionally. You should be able to write professional-quality reports,
  memos and documentation.

***If you are missing any of this background, you should not be taking this
class.***

## Course-Level Student Learning Outcomes

Upon successful completion of this course, students will be able to:

- Develop a significant software system, employing knowledge gained from
  courses throughout the program.
- Work in groups to manage a project themselves, following all appropriate
  project management techniques.
- Develop requirements, design, implementation, and quality assurance.
- Select and follow a suitable process model.
- Analyze a project for professional, social, and ethical issues.
- Learn new models, techniques, and technologies as needed and appreciate
  the necessity of such continuing professional development.

## Civic Learning, Engagement Required (CLER) Course

In this course we will be helping to develop software for
[LibreFoodPantry](https://librefoodpantry.org), a Humanitarian Free and
Open Source Software (HFOSS) project developing software to help manage
food pantries.

## LASC Student Learning Outcomes

This course does not fulfill any LASC Content Area requirements, but
contributes to the following Overarching Outcomes of LASC:

- Communicate effectively orally and in writing.
- Understand and employ quantitative and qualitative reasoning.
- Apply skills in critical thinking.
- Understand the roles of science and technology in our modern world.
- Understand how scholars in various disciplines approach problems and
  construct knowledge.
- Become socially responsible agents in the world.
- Make connections across courses and disciplines.
- Develop as healthy individuals - physically, emotionally, socially,
  ethically, and intellectually.

## Computer Science and LASC Capstone Experience

This course fulfills the Capstone Experience requirement for both the Major
in Computer Science and the LASC. From the LASC:

>Capstone seminars are offered to students in their junior or senior year
through their major field of study for varying credit or through a
three-credit course offered within the Liberal Arts and Sciences Curriculum.
Capstone seminars completed within a student's major field of study will
count toward major requirements and will meet the LASC capstone experience
requirement.

>Capstone seminars:
>
>- provide students the opportunity to demonstrate mastery of a subject
    area or skill.
>- require synthesis and integration of prior knowledge and abilities.
>- are designed to facilitate the transition from WSU to the world of work,
    professional development and/or graduate studies.
>- may include research, leadership and internship opportunities, artistic
    projects, the production of a portfolio of student work, and/or other
    culminating learning experiences.

## Software Development Concentration Student Learning Outcomes

This course addresses the following outcomes of the Software Development
Concentration of the Computer Science Major:

Graduates of the Software Development Concentration will be able to (in
addition to the Computer Science Major Program Learning Outcomes):

1. Work with stakeholders to specify, design, develop, test, modify, and
   document a software system. (Emphasis/Mastery)
2. Organize, plan, follow, and improve on, appropriate software development
   methodologies and team processes for a software project. (Introduction)
3. Evaluate, select, and use appropriate tools for source code control,
   build, test, deployment, and documentation management. (Introduction)
4. Evaluate, select, and apply appropriate testing techniques and tools,
   develop test cases, and perform software reviews. (Introduction)
5. Apply professional judgement, exhibit professional behavior, and keep
   skills up-to-date. (Mastery)

## Program-Level Student Learning Outcomes

This course addresses the following outcomes of the Computer Science Major
(see [http://www.worcester.edu/Computer-Science-Program/](http://www.worcester.edu/Computer-Science-Program/)):

Upon successful completion of the Major in Computer Science, students will
be able to:

1. Analyze a problem, develop/design multiple solutions and evaluate and
   document the solutions based on the requirements. (Mastery)
2. Communicate effectively both in written and oral form. (Mastery)
3. Identify professional and ethical considerations, and apply ethical
   reasoning to technological solutions to problems. (Mastery)
4. Demonstrate an understanding of and appreciation for the importance of
   negotiation, effective work habits, leadership, and good communication
   with teammates and stakeholders. (Mastery)
5. Learn new models, techniques, and technologies as they emerge and
   appreciate the necessity of such continuing professional development.
   (Mastery)

## Course Format

This is a course about learning to work within a large, distributed
software project. Much of our class time will be used for planning, review,
estimation, and task creation and selection, learning, and implementation
work.

## Course Objectives

This course is intended to mimic, as closely as possible, the professional
software development team environment. The students will develop a
significant software system, employing knowledge gained from courses
throughout the program. They will work in groups to manage a project,
selecting and following appropriate project management techniques. They
will develop the requirements, design, implementation, and quality
assurance plan, and analyze the project for professional, social, and
ethical issues.

The students will have to learn new technology and techniques on their own
as required for the specific project, showing the importance of continuing
professional development. They will have to reflect on their learning and
make frequent written and oral status reports on their individual and team
progress. They will be encouraged to contribute the results of their
learning and their work back to the professional community.

The students will explore and reflect on what it means to be a professional
software developer.

## Course Topics

- Team Roles
  - Scrum master
  - Frontend developer
  - Backend developer
  - Documentation Writer
  - Code Merger
  - Code Reviewer
  - Quality Assurance and Tester
  - Database Designer/Implementer
- Software Development Process and Tools
  - Using Gitlab issues, boards, epics, and milestones to plan, design,
    coordinate, implement, review, and test.
- Infrastructure Skills and Tools
  - Containers
  - Continuous Integration
  - Testing frameworks
  - Documentation System
  - Versioning
  - Working agreements
  - Dependencies
  - Tech decisions
  - Build scripts
- Product Skills
  - Designing for Product Functionality
  - Designing APIs
  - Designing GUIs
  - Writing Unit Tests
  - Writing Functional Tests
  - Writing Integration Tests
  - Writing Code for Functionality
  - Writing Documentation
- Professionalism

## Grading Policies

I want everyone receiving a passing grade in this course to be, at least,
minimally competent in the course learning outcomes and for that to be
reflected in your course grade. Traditional grading schemes do a poor job
of indicating competency.

As an example, imagine a course with two major learning outcomes: X and Y.
It is widely considered that a course grade of C indicates that a student
is minimally competent in achieving the course outcomes. However, if the
student were to receive a grade of 100 for outcome X, and a grade of 40 for
outcome Y, the student would still have a 70 (C-) average for the course.
Yet the student is clearly not competent in outcome Y.

Therefore the grading in this course will be handled in a different manner:

### Assignment Grading

- Attendance/participation, blog posts, and the final presentation will be
  graded on a ***Meets Specification*** / ***Does Not Yet Meet Specification***
  basis, based on whether the student work meets the instructor-supplied
  specification.
- Sprint Planning (team and individual scores), and Sprint Review (team score)
  will be graded on a 3-point scale. (See the Sprint Planning and Sprint Review
  sections for the specifics.)
- **For each assignment, you will be given a detailed specification explaining
  what is required for the work to be marked *Meets Specification* or the
  expected amount/types of work.**
- Failing to meet ***any part*** of the specification will result in the work
  being marked **Does Not Yet Meet Specification**.
- If you are unclear on what the specification requires, it is your
  responsibility to ask me for clarification.
- It will be possible to revise and resubmit a limited number of assignments
  with **Does Not Yet Meet Specification** grades (see *Revision and
  Resubmission of Work* below).

### Course Grade Determination

- A minimum collection of assignments, indicating *minimal* competency in the
    course learning outcomes, must be completed in a ***Meets Specification***
    manner or with a minimum number of total points to earn a passing course
    grade (D).
- Higher passing grades (A, B, C) can be earned by completing more assignments
  and/or a higher total number of points on assignments.
- See the table below:

#### Base Grade

<!-- markdownlint-disable MD033 -->
<br><br><br>Assignment | Earn<br>Base<br>Grade<br>A | Earn<br>Base<br>Grade<br>B | Earn<br>Base<br>Grade<br>C | Earn<br>Base<br>Grade<br>D
--- | :-: | :-: | :-: | :-:
Attendance and Participation (Class Sessions)<br>&nbsp;&nbsp;&mdash; Introduction, Laptop Check, Team Formation, and Working Agreements<br>&nbsp;&nbsp;&mdash; Customer Meeting and Pantry Tour<br>&nbsp;&nbsp;&mdash; Team Project Effectiveness Meetings (out of 9)<br>&nbsp;&nbsp;&mdash; Sprint Planning&nbsp;(out of 3)<br>&nbsp;&nbsp;&mdash; Sprint Review&nbsp;(out of 3)<br>&nbsp;&nbsp;&mdash; Team Work Class&nbsp;(out of 8) | <br>&#10004;<br>&#10004;<br>9<br>3<br>3<br>6 | <br>&#10004;<br>&#10004;<br>9<br>3<br>3<br>5 | <br>&#10004;<br>&#10004;<br>9<br>3<br>3<br>4 | <br>&#10004;<br>&#10004;<br>9<br>2<br>2<br>1
Team Project Effectiveness Reports (TEAMMATES) (out of 42 points) | 36 pts | 34 pts | 32 pts | 30 pts
Sprint Retrospective Blog Posts (3 Sprints) | 3 | 3 | 3 | 3
<em>Apprenticeship Patterns</em> Chapter 1 and Chapter 2-6 Introductions Blog Post | &#10004; | &#10004; | &#10004;| &#10004;
Final Presentation | &#10004; | &#10004; | &#10004; | &#10004;

- **Failing to meet the all the requirements for a particular letter grade
  will result in not earning that grade.** For example, even if you complete
  all other requirements for a B grade, but fail to attend 5 Team Work Classes,
  you will earn a C grade.
- **Failing to meet the all the requirements for earning a D grade will result
  in a failing grade for the course.**

#### Plus or Minus Grade Modifiers

- You will have a ***plus*** modifier applied to your base grade if you earn
  1 point more on your TEAMMATES reports than required for a particular base
  grade.
- You will have a ***minus*** modifier applied to your base grade if you earn
  1 point less on your TEAMMATES reports than required for a particular base
  grade.

*Note:*

- WSU has no A+ grade.

## Attendance and Participation (In-Class)

You are expected to attend all required in-class meetings. During our class
meetings, your team will be planning for upcoming work, and reviewing past
work.

For your Attendance and Participation for a particular class to be marked
***Meets Specification*** you are expected to:

- Be present.
- Have arrived on time.
- Participate fully in team activities.

### Introduction, Laptop Check, Team Formation and Working Agreement; and Customer Meeting and Pantry Tour

For the first two classes, we will be discussing the LibreFoodPantry project
and architecture, checking our development environments, forming our teams,
developing working agreements, meeting the customer, and touring Thea's
Pantry.

### Team Project Effectiveness Meetings

Most weeks your team will have a Team Project Effectiveness Meeting with the
instructor where you will go over the results of the Team Project Effectiveness
Reports that you submitted about the previous week's work. You will discuss
ways for the team and the individual team members to improve their performance.

Before each Team Project Effectiveness Meeting you will be expected to have
read the Team Project Effectiveness Report email(s) sent to you by the
TEAMMATES software. You should be prepared to provide your own summary of your
teammates' feedback, and what you plan to do to address that feedback in the
coming week.

On the day of each Team Effectiveness Meeting, your team will meet with the
instructor for 15 minutes during the class period. The remaining time in those
classes will be used for planning, backlog refinement, and general team work.

### Sprints

Your work will be organized into 3 *sprints*. A sprint is *time-boxed*, meaning
that it has a set time length, and work is planned for the sprint based on a
team-determined estimation of the difficulty of the work to be completed and
how much work the team can accomplish during the sprint.

Sprint 1 will be 4.5 weeks long and consist of 7 class periods. Sprint 2 will
be 4 weeks long and will consist of 8 class periods. These two sprints will be
used for planning and development of new features and infrastructure for your
part of the project. Each of these sprints will have a *sprint goal* setting
out the overarching purpose of the sprint, and will be used to help determine
what work will be included in the sprint.

The third sprint will be 4 weeks long and consists of 7 class periods. This
sprint will be used to finish off work, tie up loose ends and make sure that
your part of the project is ready for the next team to work on. This will
involve making sure the issues and boards are up to date and reflect the
current status, as well as any known or new issues, design decisions, etc.

Sprints will be composed of the following types of meetings:

#### Sprint Planning

The first class of each sprint will be used for the team to share the Sprint
Goal for the upcoming sprint, and the team's plans to accomplish that Sprint
Goal before the end of the Sprint.

This meeting will be facilitated by the team's Scrum Master. The Product Owner
(the instructor) will be a participant in this meeting, and will help with the
planning.

#### Backlog Refinement

In the middle of each of the first two sprints, one class will be devoted to
making sure the items on the product backlog are clearly enough specified and
sized for planning for the next spring. This meeting will be facilitated by
the team's Scrum Master. The Product Owner (the instructor) will be available
to help with the backlog refinement.

Backlog refinement can take place continuously throughout the sprint as and
when the team has time and/or information needed to improve the issues on the
backlog.

#### Sprint Review

The last class of each sprint will be used to review completed work. This
meeting will be facilitated by the team's Scrum Master. The customer and
the Product Owner (the instructor) will participate in this meeting, will
be the ones you are presenting the completed work to, and will make the
determination of whether to accept work as "done".

### Team Work Classes

The rest of the time you will work in small groups on planning, development,
and learning. The Product Owner (the instructor) may be available for
questions during these classes, depending on what other meetings are
happening.

During these classes your team should also have an informal Standup Meeting
to keep each other informed of what you have done, what you plan to do next,
and any problems you are having. This meeting will be facilitated by the
team's Scrum Master. The Product Owner (the instructor) may observe,
depending on what other meetings are happening.

## Team Project Effectiveness Reports (TEAMMATES)

### Specification

- Provided responses, feedback, and reflection are thoughtful, useful, and actionable.
- Description of work done is clear and complete, with links to evidence of what was done.

### Rubric

Description | Score
--- | ---
Clearly meets the above specification | 3 pts
Key portions do not meet the above specification | 2 pts
Clearly **does not** meet the above specification | 1 pt
Not submitted | 0 pts

## Blog Posts

You will be required to keep a blog about your part of the project and your
experiences in this class, as well as summaries and reflections on the
readings. Your blog must be publicly accessible[^1], and will be aggregated
on the [CS@Worcester Blog](http://cs.worcester.edu/blog/) and (possibly) the
[Teaching Open Source](http://teachingopensource.org/) Planet.

Your blog posts will be about your learning in the class, the work and work
products you are producing, the readings, etc.

[^1]: If there is a reason why your name cannot be publicly associated with
this course, you may blog under a pseudonym. You must see me to discuss the
details, but your blog must still be publicly accessible and aggregated, and
you must inform me of your pseudonym.

### Sprint Retrospective Blog Posts

At the end of each sprint, you will post a Sprint Retrospective blog post
about your individual learning in the class, the work and work products you
are producing, etc. (Details below in the `Blog Posts` section.)

For your Sprint Retrospective Blog Posts to be marked
***Meets Specification*** your blog post is expected to contain:

- Links to evidence of activity on GitLab with a one sentence description
  for each.
- Reflection on what worked well.
- Reflection on what didn't work well.
- Reflection on what changes could be made to improve as a team.
- Reflection on what changes could be made to improve as an individual.
- The selection of a *single* pattern from the Apprenticeship Patterns book.
  - This pattern will be selected from the
  [35 patterns in the book](https://www.oreilly.com/library/view/apprenticeship-patterns/9780596806842/apa.html) divided among Chapters 2-6,
  chosen for its relevance to your experience during the sprint,
  - A *short* summary of the pattern.
  - Why did you select this pattern? What is its relevance to your experience
  during the spring?
  - How would having read the pattern have changed your behavior during the
  sprint?
- The tag for the Sprint number, E.g. Sprint-1.
- 500-750 words.

### *Apprenticeship Patterns* Chapter 1 and Chapter 2-6 Introductions

Everyone must read and post about Chapter 1 of
[*Apprenticeship Patterns: Guidance for the Aspiring Software Craftsman* by Dave Hoover and Adewale Oshineye](https://www.oreilly.com/library/view/apprenticeship-patterns/9780596806842/#toc-start),
and the introduction portions of Chapters 2-6.

The introduction portions of Chapters 2-6 are the portions from the beginning
of the chapter to start of the first pattern.

The objectives of this assignment are for students to understand what it means
to be a Software Craftsman, and the three stages of becoming a Software
Craftsman. In addition, the students will read the introductions to the
remaining 5 chapters to get a feel for what the patterns in each chapter cover,
and to help them choose which individual patterns they will read a blog about
later in the course.

For your *Apprenticeship Patterns* Chapter 1 and Chapter 2-6 Introductions
Blog Post to be marked ***Meets Specification*** your blog post is expected
to contain:

- Your reaction to the reading, not simply a summary of the material.
  - What did you find interesting, useful, thought-provoking about the reading?
  - Has the reading caused you to change your opinion, the way you think about
    the topic, or how you work? 
  - Do you disagree with something in the reading? And why?
  - Which chapters seem most relevant to you?
- 350-500 words.
- Tagged as `CS-448`.
- Tagged appropriately to appear on the `CS@Worcester Blog`.

## Final Presentation

Final presentations by teams on their work and experience will be given during
our scheduled final exam period:

- Section 01: Monday, 12 May 2025 &mdash; 12:30-15:30
- Section 02: Tuesday, 13 May 2025 &mdash; 8:30-11:30
  
## Deliverables

All work will be submitted electronically through a variety of tools. The due
date and time will be given on the assignment. The submission date and time
will be determined by the submission timestamp of the tool used.

**Please do not submit assignments to me via email.** It is difficult for me
to keep track of them and I often fail to remember that they are in my mailbox
when it comes time to grade the assignment.

## Late Submissions

Late work will not be accepted. (See *Tokens* below.)

## Revision and Resubmission of Work

Because your first attempt at producing acceptable work for a new category of 
assignment may be difficult due to being unfamiliar with the specification and
not having had any feedback yet, you may revise and resubmit ***a single***
assignment in each of the following categories *only*:

- *Apprenticeship Patterns* Chapter 1 and Chapter 2-6 Introductions Blog Post
- Sprint Retrospective Blog Posts

## Tokens

Each student will be able to earn up to 5 tokens over the course of the
semester. These tokens will be earned by completing simple set-up and
housekeeping tasks for the course.

Tokens can be used to:

- Replace class sessions
  - Introduction and Team Formation class &mdash; 2 tokens
  - Tools and Workflow class &mdash; 2 tokens
  - Sprint Planning classes &mdash; 3 tokens per class
  - Sprint Review classes &mdash;  3 tokens per class
  - Team Project Effectiveness Meeting class &mdash; 2 tokens per class
  - Team Work Class &mdash; 1 token per class

**Tokens cannot be used for any other purpose and unused tokens remaining
at the end of the semester cannot be used to change your final course grade.**

### Token Accounting

- Unused tokens will be kept track of in the Blackboard *My Grades* area.
- Tokens will not be automatically applied. You must explicitly tell me
  **by email** when you want to use a token, and for which assignment.

## Getting Help

If you are struggling with the material or a project please see me as soon as
possible. Often a few minutes of individual attention is all that is needed to
get you back on track.

By all means, try to work out the material on your own, but ask for help when
you cannot do that in a reasonable amount of time. The longer you wait to ask
for help, the harder it will be to catch up.

**Asking for help or coming to see me during office hours is not bothering
or annoying me. I am here to help you understand the material and be successful
in the course.**

## Contacting Me

You may contact me by email (Karl.Wurst@worcester.edu), telephone
(+1-508-929-8728), or see me in my office. My office hours are listed on
the schedule on my web page ([http://www.karl.w-sts.com/](http://www.karl.w-sts.com/))
or you may make an appointment for a mutually convenient time.

**If you email me, please include “[CS-448]” in the subject line, so that my
email program can correctly file your email and ensure that your message does
not get buried in my general mailbox.**

**You must email me only from your Worcester State email due to privacy
concerns.**

My goal is to get back to you within 24 hours of your email or phone call (with
the exception of weekends and holidays), although you will likely hear from me
much sooner. If you have not heard from me after 24 hours, please remind me.

![Cartoon with bad examples of how to send email to your instructor](http://www.phdcomics.com/comics/archive/phd042215s.gif)
[http://www.phdcomics.com/comics.php?f=1795](http://www.phdcomics.com/comics.php?f=1795)

## Code of Conduct/Classroom Civility

All students are expected to adhere to the policies as outlined in the
University’s Student Code of Conduct.

## Student Responsibilities

- Contribute to a class atmosphere conducive to learning for everyone by
  asking/answering questions, participating in class discussions. Don’t
  just lurk!
- When working with a partner, participate actively. Don't let your partner
  do all the work - you won't learn anything that way.
- Seek help when necessary
- Start assignments as soon as they are posted.  Do not wait until the due
  date to seek help/to do the assignments
- Make use of the student support services (see below)
- Expect to spend at least 9 hours of work per week on classwork.
- Each student is responsible for the contents of the readings, handouts,
  and homework assignments.

## Preferred Names and Pronouns

If you prefer to be addressed, or referred to, by a different name than
appears on your student record and/or specify your pronouns, please let me
know.

If you wish this information to appear on class lists for all of your
professors, you may wish to complete the
[Student Chosen Name, Gender Identity, and Pronoun Usage Request Form](https://www.worcester.edu/campus-life/student-services/registrar/forms-faqs/)
and return it to the Registrar's Office.

## Additional Institutional Information

Academic Affairs maintains documents with additional information about
policies and services available to students. You may access them through
the link in Blackboard.

## Academic Conduct

Each student is responsible for the contents of the readings, discussions,
class materials, textbook and handouts.

**You should familiarize yourself with Worcester State College’s Academic
Honesty policy. The policy outlines what constitutes academic dishonesty,
what sanctions may be imposed and the procedure for appealing a decision.
The complete Academic Honesty Policy appears at:
[http://www.worcester.edu/Academic-Policies/](http://www.worcester.edu/Academic-Policies/)**

**If you have a serious problem that prevents you from finishing an
assignment on time, contact me and we'll come up with a solution.**

## Acknowledgements

This course is heavily influenced by ongoing discussions with
[Dr. Stoney Jackson](https://wne.edu/directory/herman-jackson) of Western
New England University beginning during the Fall 2014 semester (with 64
commuting hours in his car, lunches, attending his CS-390 class, and more)
and still continuing... It is also based on materials from and discussions
with [Dr. Heidi Ellis](https://wne.edu/directory/heidi-ellis) of Western
New England University and 
[Dr. Grant Braught](https://www.dickinson.edu/site/custom_scripts/dc_faculty_profile_index.php?fac=braught) of Dickinson College.

## Student Work Retention Policy

It is my policy to securely dispose of student work one calendar year after
grades have been submitted for a course.

## Schedule

*The following course schedule is subject to change.*

### Section 01 - Mondays and Wednesdays

Date | Topic
--- | ---
Monday, 20 January | **&#9940; No Class**<br>*Martin Luther King Jr. Day*
Wednesday, 22 January | **Introduction, Laptop Check, Team Formation, and Working Agreements - Required**
Monday, 27 January | **Customer Meeting and Pantry Tour - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 29 January | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Pre-Planning
Monday, 3 February | **&#128197; Sprint 1 Planning - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 5 February | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 10 February | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 12 February | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 17 February | **&#9940; No Class**<br>*Presidents Day*<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 19 February | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Backlog Refinement<br>Continue working in teams
Monday, 24 February | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 26 February | **&#9940; No Class**<br>*I will be at a conference*
Monday, 3 March | **&#128203;&#9989; Sprint 1 Review - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 5 March | **&#128197; Sprint 2 Planning - Required**
Monday, 10 March |  Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 12 March | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 17 March | **&#9940; No Class**<br>*Spring Break*
Wednesday, 19 March | **&#9940; No Class**<br>*Spring Break*
Monday, 24 March | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 26 March | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 31 March | Team Work Class &mdash; Backlog Refinement<br>Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 2 April | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 7 April | **&#128203;&#9989; Sprint 2 Review - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 9 April | **&#128197; Sprint 3 Planning - Required** 
Monday, 14 April | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 16 April | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 21 April | **&#9940; No Class**<br>*Patriots Day*<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 23 April | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Monday, 28 April | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 30 April | Team Work Class &mdash; Continue working in teams
Monday, 5 May | **&#128203;&#9989; Sprint 3 Review - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Wednesday, 7 May | **&#9940; No Class**<br>*Professional Development Day* | **&#9940; No Class**<br>*Professional Development Day*
Monday, 12 May | **Final Presentations - Required**<br>*12:30-15:30*

### Section 02 - Tuesdays and Thursdays

Date | Topic
--- | ---
Tuesday, 21 January | **&#9940; No Class**<br>*We have 1 more class than MW*
Thursday, 23 January | **Introduction, Laptop Check, Team Formation, and Working Agreements - Required**
Tuesday, 28 January | **Customer Meeting and Pantry Tour - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 30 January | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Pre-Planning
Tuesday, 4 February | **&#128197; Sprint 1 Planning - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 6 February | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 11 February | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 13 February | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 18 February | Team Work Class &mdash; Backlog Refinement<br>Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 20 February | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 25 February | **&#9940; No Class**<br>*I will be at a conference*<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 27 February | **&#9940; No Class**<br>*I will be at a conference*
Tuesday, 4 March | **&#128203;&#9989; Sprint 1 Review - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 6 March | **&#128197; Sprint 2 Planning - Required**
Tuesday, 11 March | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 13 March | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 18 March | **&#9940; No Class**<br>*Spring Break*
Thursday, 20 March | **&#9940; No Class**<br>*Spring Break*
Tuesday, 25 March | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 27 March | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 1 April | Team Work Class &mdash; Backlog Refinement<br>Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 3 April | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 8 April | **&#128203;&#9989; Sprint 2 Review - Required**<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 10 April | **&#128197; Sprint 3 Planning - Required** 
Tuesday, 15 April | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 17 April | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 22 April | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 24 April | **&#128200;&#128101; Team Project Effectiveness Meeting - Required**<br>Continue working in teams
Tuesday, 29 April | Team Work Class &mdash; Continue working in teams<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 1 May | **&#128203;&#9989; Sprint 3 Review - Required**
Tuesday, 6 May | **&#9940; No Class**<br>*Reading Day*<br>**&#128221; Team Project Effectiveness Report Due**
Thursday, 8 May | **&#9940; No Class**<br>*Final Exams*
Tuesday, 13 May | **Final Presentations - Required**<br>*8:30-11:30*

&copy; 2025 Karl R. Wurst and Stoney Jackson

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019
