# Final Presentations Specification

## *CS-448 01, OL &mdash; Spring 2024*

### Version 2024-Spring-1.0-Final, Revised 17 April 2024

## Final presentations by teams on their work and experience

## Length of Presentation

- Each member of the team is expected to present for 5-10 minutes.
- Each member of the team is expected to present for an
    approximately equal amount of time.
- There will be additional time after your presentation for questions
    from the audience.

## Format of Presentation

- Each member's portion of the presentation should be of
    approximately equal depth and level of technical detail. In other
    words, no member of the team should be relegated to only introducing
    the presentation, or other content-light topics.
- Your presentation is expected to be an oral presentation supported
    by appropriate visual materials:
  - Slides of information about group process, lessons learned,
        suggestions for future semesters, etc.
  - Screenshots of the changes you made to the application, your
        scrum board, your repository, issues, pull requests, Discord
        discussion, etc.
  - Sections of code with appropriate commentary on changes made,
        etc.
  - Diagrams of repository organization, code architecture, etc.
  - A demonstration of your working portion of the system. (Consider
        using screenshots or showing a video so that you are not
        fumbling through using the program. At the very least, start up
        the program before your presentation begins, so we are not
        waiting for it to start.)
  - Etc.
- You are expected to speak clearly, at a reasonable pace, and at a
    reasonable volume.
- You are expected to speak to and make eye contact with the audience,
    not speak to the screen, to the floor, to your teammates, or only to
    the instructor.
- Your presentation may contain comments aimed at improvements to the
    course for future semesters, but such comments should be less than
    20% of the overall presentation. (The appropriate place for lengthy
    suggestions for improvement is on the anonymous survey that will be
    posted on Blackboard.)

## Suggestions

### Suggestions from Clean Code

- Assign each specific topic to a single team member to present,
    rather than having multiple team members present a single topic.
    (The *Single Responsibility Principle* applies in presentations as
    well...)
- A specific topic should be covered once, and not come up in multiple
    parts of the presentation. (The *Don't Repeat Yourself* rule applies
    here too...)
- Organize your presentation like a newspaper story - give a high-level
    overview of your presentation topics first, then cover topics in
    more depth.
- A slide, like a method, should be short and easy to read, without
    lots of unnecessary comments. Fill in the details as you speak about
    the material.
- Keep in mind your audience - use vocabulary that will make sense to
    them and make your presentation easy to follow.

### Other Suggestions

- You should focus on what you learned, not just talk about what you
    did.
- Re-read your Sprint Retrospective Blog Posts for material that you
    may have forgotten about.
- Practice your presentation in advance so that you:
  - know what you are going to say
  - don't speak too quickly or too slowly
  - can get feedback from your teammates
  - don't use too much or too little time
- Make sure your slides are readable - in general your text should not
    be smaller than 20pt, you should not have too much text on one
    slide. Make sure your slides can be read from the back of the room.
- You should not have more than 1 slide per minute, except for title
    slides or image slides for which you will not say anything.

&copy; 2024 Karl R. Wurst

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019
